package io.github.winx64.faker;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_7_R4.CraftServer;
import org.bukkit.craftbukkit.v1_7_R4.entity.CraftPlayer;
import org.bukkit.entity.Player;

import io.github.winx64.faker.data.CachedSkinData;
import io.github.winx64.faker.data.SkinCache;
import io.github.winx64.faker.reflection.PlayerReflection;
import net.minecraft.server.v1_7_R4.DedicatedPlayerList;
import net.minecraft.server.v1_7_R4.EntityPlayer;
import net.minecraft.server.v1_7_R4.PacketPlayOutPlayerInfo;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.properties.Property;

/**
 * Utility class
 * 
 * @author WinX64
 *
 */
public final class Util {

	/**
	 * Pattern of an invalid name. Valid names only contain letters, numbers and
	 * underscore(_)
	 */
	private static final Pattern INVALID_NAME_PATTERN = Pattern.compile("[^a-zA-Z0-9_]");

	private Util() {}

	/**
	 * Updates the player name, display name and player list name
	 * 
	 * @param player
	 *            The player
	 * @param newName
	 *            The new name
	 * @param newDisplayName
	 *            The new display name
	 * @param newPlayerListName
	 *            The new player list name
	 */
	public static void updateName(Player player, String newName, String newDisplayName, String newPlayerListName) {
		CraftPlayer cPlayer = (CraftPlayer) player;

		PlayerReflection.setGameProfile(cPlayer.getProfile(), newName);

		player.setDisplayName(newDisplayName);
		player.setPlayerListName(newPlayerListName);
	}

	/**
	 * Updates the tab info entry and respawns the entity model
	 * 
	 * @param cache
	 *            The skin cache
	 * @param player
	 *            The player
	 * @param performSkinLookup
	 *            Should the server attempt to lookup if the skin is not in the
	 *            local cache?
	 */
	public static void updateEntityModel(SkinCache cache, Player player, boolean performSkinLookup) {
		CraftPlayer cPlayer = (CraftPlayer) player;
		EntityPlayer nmsPlayer = cPlayer.getHandle();

		PacketPlayOutPlayerInfo removeTabData = PacketPlayOutPlayerInfo.removePlayer(nmsPlayer);
		PacketPlayOutPlayerInfo addTabData = PacketPlayOutPlayerInfo.addPlayer(nmsPlayer);

		CachedSkinData skinData = cache.getCachedSkinData(player.getName());
		cPlayer.getProfile().getProperties().clear();
		if (skinData == null) {
			if (performSkinLookup) {
				cache.retrieveSkinData(player, player.getName());
			}
		} else {
			if (skinData.getProperty() != null) {
				cPlayer.getProfile().getProperties().put("textures", skinData.getProperty());
			}
		}

		for (Player target : Bukkit.getOnlinePlayers()) {
			EntityPlayer nmsTarget = ((CraftPlayer) target).getHandle();
			nmsTarget.playerConnection.sendPacket(removeTabData);
			nmsTarget.playerConnection.sendPacket(addTabData);

			if (target != player) {
				target.hidePlayer(player);
				target.showPlayer(player);
			}
		}
	}

	/**
	 * Checks if a name is valid
	 * 
	 * @param name
	 *            The name
	 * @return If the name contains all valid and is not longer than 16
	 *         characters
	 */
	public static boolean isNameValid(String name) {
		return name.length() <= 16 && !INVALID_NAME_PATTERN.matcher(name).find();
	}

	/**
	 * Checks if the current name is in use
	 * 
	 * @param name
	 *            The name
	 * @return If it is being used
	 */
	public static boolean isNameInUse(String name) {
		return Bukkit.getPlayerExact(name) != null;
	}

	/**
	 * 
	 * Checks if the current player list name is in use
	 * 
	 * @param playerListName
	 *            The player list name
	 * @return If it is being used
	 */
	@SuppressWarnings("unchecked")
	public static boolean isPlayerListNameInUse(String playerListName) {
		DedicatedPlayerList playerList = ((CraftServer) Bukkit.getServer()).getHandle();
		for (EntityPlayer nmsPlayer : (List<EntityPlayer>) playerList.players) {
			if (nmsPlayer.listName.equals(playerListName)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the skin data property from the profile of the specified player
	 * 
	 * @param player
	 *            The player
	 * @return The skin data property
	 */
	public static Property getTextureProperty(Player player) {
		CraftPlayer cPlayer = (CraftPlayer) player;
		GameProfile profile = cPlayer.getProfile();
		Collection<Property> textures = profile.getProperties().get("textures");

		return textures.size() < 1 ? null : textures.iterator().next();
	}

	/**
	 * Returns the current unix timestamp (seconds since 01/01/1970)
	 * 
	 * @return The timestamp
	 */
	public static int unixTimestamp() {
		return (int) (System.currentTimeMillis() / 1000);
	}
}
