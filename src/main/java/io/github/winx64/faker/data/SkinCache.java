package io.github.winx64.faker.data;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;

import io.github.winx64.faker.NameFaker;
import io.github.winx64.faker.Util;
import io.github.winx64.faker.data.FindResult.Result;
import io.github.winx64.faker.player.FakePlayer;
import net.minecraft.server.v1_7_R4.MinecraftServer;
import net.minecraft.util.com.mojang.authlib.Agent;
import net.minecraft.util.com.mojang.authlib.GameProfile;
import net.minecraft.util.com.mojang.authlib.ProfileLookupCallback;
import net.minecraft.util.com.mojang.authlib.properties.Property;

/**
 * Handles everything related to the database and skin cache
 * 
 * @author WinX64
 *
 */
public final class SkinCache {

	private static final String CONFIG_FILE_NAME = "config.yml";

	/**
	 * SQL instructions to create the default tables
	 */
	private static final String SKIN_CACHE_TABLE_SQL = "CREATE TABLE IF NOT EXISTS `skin_cache` ("
			+ "`name` CHAR(16) NOT NULL," + "`timestamp` INT NOT NULL," + "`skin_data` TEXT NOT NULL,"
			+ "PRIMARY KEY (`name`))" + "ENGINE = InnoDB";

	private final NameFaker plugin;
	private final File configFile;

	private Cache<String, CachedSkinData> cachedSkinData;

	private String host;
	private String port;
	private String username;
	private String password;
	private String databaseName;

	private int databaseCacheExpiry;
	private int localCacheExpiry;
	private int localCacheSizeLimit;

	private Connection connection;

	public SkinCache(NameFaker plugin) {
		this.plugin = plugin;
		this.configFile = new File(plugin.getDataFolder(), CONFIG_FILE_NAME);

		this.host = "localhost";
		this.port = "3306";
		this.username = "root";
		this.password = "password";
		this.databaseName = "name_faker";

		this.databaseCacheExpiry = 86400;
		this.localCacheExpiry = 3600;
		this.localCacheSizeLimit = 50;
	}

	/**
	 * Loads the skin cache configuration
	 * 
	 * @return Whether it was successful or not
	 */
	public boolean loadConfiguration() {
		try {
			plugin.log("[Config] Loading configuration...");
			if (!configFile.exists()) {
				plugin.log(Level.WARNING, "[Config] Inexistent configuration. Creating the default one...");
				this.plugin.saveResource(CONFIG_FILE_NAME, true);
			}

			// Load the configuration manually to avoid defaults
			FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);

			ConfigurationSection databaseSection = config.getConfigurationSection("database-connection");
			if (databaseSection == null) {
				plugin.log(Level.WARNING,
						"[Config] The 'database-connection' section is missing from the configuration. Assuming default values!");
			} else {
				if (databaseSection.contains("host")) {
					this.host = databaseSection.getString("host");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'host' on configuration file. Using default %s", host);
				}
				if (databaseSection.contains("port")) {
					this.port = databaseSection.getString("port");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'port' on configuration file. Using default %s", port);
				}
				if (databaseSection.contains("username")) {
					this.username = databaseSection.getString("username");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'username' on configuration file. Using default %s", username);
				}
				if (databaseSection.contains("password")) {
					this.password = databaseSection.getString("password");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'password' on configuration file. Using default %s", password);
				}
				if (databaseSection.contains("database")) {
					this.databaseName = databaseSection.getString("database");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'database' on configuration file. Using default %s", databaseName);
				}
			}

			ConfigurationSection cacheSection = config.getConfigurationSection("cache-properties");
			if (cacheSection == null) {
				plugin.log(Level.WARNING,
						"[Config] The 'cache-configuration' section is missing from the configuration. Assuming default values!");
			} else {
				if (cacheSection.contains("database-cache-expiry")) {
					this.databaseCacheExpiry = cacheSection.getInt("database-cache-expiry");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'database-cache-expiry' on configuration file. Using default %d",
							databaseCacheExpiry);
				}
				if (cacheSection.contains("local-cache-expiry")) {
					this.localCacheExpiry = cacheSection.getInt("local-cache-expiry");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'local-cache-expiry' on configuration file. Using default %d",
							localCacheExpiry);
				}
				if (cacheSection.contains("local-cache-size-limit")) {
					this.localCacheSizeLimit = cacheSection.getInt("local-cache-size-limit");
				} else {
					this.plugin.log(Level.WARNING,
							"[Config] Missing option 'local-cache-size-limit' on configuration file. Using default %d",
							localCacheSizeLimit);
				}
			}

			// Initializes the local skin cache
			this.cachedSkinData = CacheBuilder.newBuilder().maximumSize(this.localCacheSizeLimit)
					.expireAfterAccess(this.localCacheExpiry, TimeUnit.SECONDS).build(new SkinDataCacheLoader());

			plugin.log("[Config] Configuration loaded with success!");
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e, "[Config] An error occurred while loading the configuration! Details below:");
			return false;
		}
	}

	/**
	 * Initializes database related procedures. Includes creating the required
	 * tables and testing the connection
	 * 
	 * @return Whether it was successful or not
	 */
	public boolean initialize() {
		Statement createTable = null;
		try {
			plugin.log("[DB] Opening connection with the server...");
			this.connection = this.getConnection();

			plugin.log("[DB] Connection established! Creating default tables...");
			createTable = connection.createStatement();
			createTable.executeUpdate(SKIN_CACHE_TABLE_SQL);
			plugin.log("[DB] Tables created! Ready to use!");
			return true;
		} catch (Exception e) {
			plugin.log(Level.SEVERE, e,
					"[DB] An error occurred while initializing the database and its default data! Details below:");
			return false;
		} finally {
			if (createTable != null) {
				try {
					createTable.close();
				} catch (SQLException e) {}
			}
		}
	}

	/**
	 * Closes the connection
	 */
	public synchronized void dispose() {
		try {
			if (connection != null && !connection.isClosed()) {
				this.connection.close();
			}
		} catch (Exception e) {}
	}

	/**
	 * Adds the specified player skin data to the local skin cache. <b>This does
	 * NOT add the skin data to the database cache</b>
	 * 
	 * @param player
	 *            The player
	 */
	public void cacheSkinData(Player player) {
		Property prop = Util.getTextureProperty(player);
		this.cachedSkinData.asMap().put(player.getName().toLowerCase(), new CachedSkinData(Util.unixTimestamp(), prop));
	}

	/**
	 * Returns the skin data on the local skin cache. <b>The server will NOT
	 * attempt to get the skin data from the database cache if this fails</b>
	 * 
	 * @param name
	 *            The name of the player
	 * @return The skin data, or null
	 */
	public CachedSkinData getCachedSkinData(String name) {
		return cachedSkinData.asMap().get(name.toLowerCase());
	}

	/**
	 * Attempts to retrieve the skin data from the database cache. If it doesn't
	 * exist, it will attempt to retrieve the skin data from Mojang's servers.
	 * If it is successful, the retrieved skin is saved on the database cache
	 * and added to the local cache, and the specified player updated.
	 * 
	 * @param player
	 *            The player
	 * @param name
	 *            The player name
	 */
	public void retrieveSkinData(Player player, String name) {
		Bukkit.getScheduler().runTaskAsynchronously(plugin, new SkinDataRetriever(player, name));
	}

	/**
	 * Tries to open a connection with the MySQL server
	 * 
	 * @throws SQLException
	 *             If something goes wrong
	 */
	private synchronized void openConnection() throws SQLException {
		this.connection = DriverManager.getConnection(
				String.format("jdbc:mysql://%s:%s/%s", host, port, databaseName), username, password);
	}

	/**
	 * Gets the connection with the MySQL server. If no open connection exists,
	 * it will attempt to connect and return the connection
	 * 
	 * @return The connection
	 * @throws SQLException
	 *             If something goes wrong
	 */
	private synchronized Connection getConnection() throws SQLException {
		if (this.connection == null || this.connection.isClosed()) {
			this.openConnection();
		}
		return connection;
	}

	/**
	 * Deletes the skin data from the database cache
	 * 
	 * @param name
	 *            The player name
	 * @return Whether it was successful or not
	 */
	public synchronized boolean removeSkinData(String name) {
		PreparedStatement deleteStatement = null;
		try {
			deleteStatement = this.getConnection().prepareStatement("DELETE FROM `skin_cache` WHERE `name`=?");
			deleteStatement.setString(1, name.toLowerCase());
			deleteStatement.executeUpdate();
			return true;
		} catch (Exception e) {
			plugin.log(Level.WARNING, e,
					"[DB] An error while trying to delete the cached data for '%s'! Details below:", name);
			return false;
		} finally {
			if (deleteStatement != null) {
				try {
					deleteStatement.close();
				} catch (SQLException e) {}
			}
		}
	}

	/**
	 * Saves the skin data to the database cache
	 * 
	 * @param name
	 *            The player name
	 * @param skinData
	 *            The skin data
	 * @return Whether it was successful or not
	 */
	public synchronized boolean saveSkinData(String name, CachedSkinData skinData) {
		PreparedStatement insertStatement = null;
		try {
			insertStatement = this.getConnection().prepareStatement(
					"INSERT INTO `skin_cache` VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `timestamp`=?, `skin_data`=?");
			insertStatement.setString(1, name.toLowerCase());

			String compressedData = skinData.getCompressedData();
			for (int i = 0; i < 2; i++) {
				insertStatement.setInt(i * 2 + 2, Util.unixTimestamp());
				insertStatement.setString(i * 2 + 3, compressedData);
			}
			insertStatement.executeUpdate();
			return true;
		} catch (Exception e) {
			plugin.log(Level.WARNING, e, "[DB] An error while trying to save the cached data for '%s'! Details below:",
					name);
			return false;
		} finally {
			if (insertStatement != null) {
				try {
					insertStatement.close();
				} catch (SQLException e) {}
			}
		}
	}

	/**
	 * Retrieves the skin data from the database cache
	 * 
	 * @param name
	 *            The player name
	 * @return The result of the operation. <b>This value will NEVER be null</b>
	 */
	public synchronized FindResult<CachedSkinData> findSkinDataByName(String name) {
		PreparedStatement selectStatement = null;
		ResultSet resultSet = null;
		try {
			selectStatement = this.getConnection().prepareStatement("SELECT * FROM `skin_cache` WHERE `name` = ?");
			selectStatement.setString(1, name.toLowerCase());
			resultSet = selectStatement.executeQuery();

			if (!resultSet.first()) {
				return FindResult.notFound();
			}

			int timestamp = resultSet.getInt("timestamp");
			String compressedData = resultSet.getString("skin_data");

			return FindResult.found(new CachedSkinData(timestamp, compressedData));
		} catch (Exception e) {
			plugin.log(Level.WARNING, e,
					"[DB] An error while trying to retrieve the cached data for '%s'! Details below:", name);
			return FindResult.error();
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {}
			}
			if (selectStatement != null) {
				try {
					selectStatement.close();
				} catch (SQLException e) {}
			}
		}
	}

	/**
	 * Helper CacheLoader class to retrieve the skin from the database cache or
	 * Mojang's server
	 * 
	 * @author WinX64
	 *
	 */
	private final class SkinDataCacheLoader extends CacheLoader<String, CachedSkinData> {

		@Override
		public CachedSkinData load(String name) throws Exception {
			// Try to find the skin data on the database cache
			FindResult<CachedSkinData> result = findSkinDataByName(name);

			// If the specified data exists
			if (result.getResult() == Result.FOUND) {

				// Is it still valid?
				if (result.getData().getTimestamp() + databaseCacheExpiry > Util.unixTimestamp()) {

					// It is valid. Return the found skin data
					return result.getData();
				}

				// It is not. Remove it from the database skin cache and
				// continue...
				removeSkinData(name);
			}

			// Try to get the player's profile from Mojang's servers
			SkinLookupCallback callback = new SkinLookupCallback();
			MinecraftServer.getServer().getGameProfileRepository().findProfilesByNames(new String[] { name },
					Agent.MINECRAFT, callback);

			// Does the player exist?
			if (callback.ex != null) {
				// It doesn't, stop right here
				plugin.log(Level.WARNING, "[Skin] The player '%s' doesn't exist!", name);
				return new CachedSkinData();
			}

			// The player exists, try retrieve the rest of the data
			GameProfile profile = callback.profile;
			MinecraftServer.getServer().av().fillProfileProperties(profile, true);
			Collection<Property> textures = profile.getProperties().get("textures");

			// Does the profile contains the skin data?
			if (textures.size() < 1) {
				// This will never happen if the retrieval was successful, but
				// will happen if the request is denied by the Mojang's server,
				// one of the reasons being sending requests way too fast
				plugin.log(Level.WARNING, "[Skin] Skin data for '%s' is empty!", name);
				return new CachedSkinData();
			}

			// All good, create a skin cache object with the current machine
			// timestamp and save it to the database cache
			CachedSkinData skinData = new CachedSkinData(Util.unixTimestamp(), textures.iterator().next());
			saveSkinData(name, skinData);
			return skinData;
		}

		/**
		 * Helper callback class to handle profile searches
		 * 
		 * @author WinX64
		 *
		 */
		private final class SkinLookupCallback implements ProfileLookupCallback {

			private GameProfile profile;
			private Exception ex;

			@Override
			public void onProfileLookupSucceeded(GameProfile profile) {
				this.profile = profile;
			}

			@Override
			public void onProfileLookupFailed(GameProfile profile, Exception ex) {
				this.profile = profile;
				this.ex = ex;
			}
		}
	}

	/**
	 * Helper class to retrieve and add the skin data to the local cache.
	 * <b>Should be always called asynchrounously</b>
	 * 
	 * @author WinX64
	 *
	 */
	private final class SkinDataRetriever implements Runnable {

		private final Player player;
		private final String name;

		private SkinDataRetriever(Player player, String name) {
			this.player = player;
			this.name = name;
		}

		@Override
		public void run() {
			try {
				// Returns the cached skin data, and forces the cache to perform
				// an expensive lookup on the database cache or Mojang's servers
				// if the data is not present on it
				CachedSkinData skinData = cachedSkinData.get(name.toLowerCase());
				if (skinData.getProperty() == null) {
					return;
				}

				// If the method above does not throw an exception, it means all
				// went well.
				// Schedule a task to find the player who requested the skin and
				// set it on them, if they're still online
				Bukkit.getScheduler().runTask(plugin, new Runnable() {
					@Override
					public void run() {
						if (!player.isOnline()) {
							return;
						}

						FakePlayer fPlayer = plugin.getFakePlayer(player);
						if (fPlayer == null) {
							return;
						}

						Util.updateEntityModel(SkinCache.this, player, false);
					}
				});
			} catch (Exception e) {}
		}
	}
}
