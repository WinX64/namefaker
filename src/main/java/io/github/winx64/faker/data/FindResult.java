package io.github.winx64.faker.data;

/**
 * The result of a database data search
 * 
 * @author WinX64
 *
 */
public final class FindResult<T> {

	private final Result result;
	private final T data;

	private FindResult(Result result, T data) {
		this.result = result;
		this.data = data;
	}

	public Result getResult() {
		return result;
	}

	public T getData() {
		return data;
	}

	public static <T> FindResult<T> found(T data) {
		return new FindResult<T>(Result.FOUND, data);
	}

	public static <T> FindResult<T> notFound() {
		return new FindResult<T>(Result.NOT_FOUND, null);
	}

	public static <T> FindResult<T> error() {
		return new FindResult<T>(Result.ERROR, null);
	}

	public static enum Result {
		FOUND, NOT_FOUND, ERROR;
	}
}
