package io.github.winx64.faker.data;

import io.github.winx64.faker.Util;
import net.minecraft.util.com.google.gson.Gson;
import net.minecraft.util.com.google.gson.JsonObject;
import net.minecraft.util.com.mojang.authlib.properties.Property;

/**
 * Holds skin data that has been cached. Holds the skin itself and a timestamp
 * of the moment it was cached
 * 
 * @author WinX64
 *
 */
public final class CachedSkinData {

	private final int timestamp;
	private final Property property;
	private String compressedProperty;

	public CachedSkinData() {
		this.timestamp = Util.unixTimestamp();
		this.property = null;
	}

	public CachedSkinData(int timestamp, Property property) {
		this.timestamp = timestamp;
		this.property = property;
		this.compressedProperty = null;
	}

	public CachedSkinData(int timestamp, String jsonData) {
		JsonObject properties = new Gson().fromJson(jsonData, JsonObject.class);

		String name = properties.get("name").getAsString();
		String base = properties.get("value").getAsString();
		String signature = properties.get("signature").getAsString();

		this.timestamp = timestamp;
		this.property = new Property(name, base, signature);
		this.compressedProperty = null;

	}

	public int getTimestamp() {
		return timestamp;
	}

	public Property getProperty() {
		return property;
	}

	public static void main(String[] args) {
		Property property = new Property("MyName", "MyValue", "MySignature");
		JsonObject textureObject = new JsonObject();

		textureObject.addProperty("name", property.getName());
		textureObject.addProperty("value", property.getValue());
		textureObject.addProperty("signature", property.getSignature());

		System.out.println(textureObject.toString());
	}

	public String getCompressedData() {
		if (compressedProperty == null) {
			JsonObject textureObject = new JsonObject();

			textureObject.addProperty("name", property.getName());
			textureObject.addProperty("value", property.getValue());
			textureObject.addProperty("signature", property.getSignature());

			return textureObject.toString();
		}

		return compressedProperty;
	}
}
