package io.github.winx64.faker.player;

import org.bukkit.entity.Player;

/**
 * Wrapper class for the player, also holds some extra information
 * 
 * @author WinX64
 *
 */
public final class FakePlayer {

	private final Player player;

	private final String originalName;
	private String currentFakeName;

	public FakePlayer(Player player) {
		this.player = player;

		this.originalName = player.getName();
		this.currentFakeName = null;
	}

	/**
	 * Returns the Player instance linked to this FakePlayer
	 * 
	 * @return The Player
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * Returns the original name of this player upon login
	 * 
	 * @return The original name
	 */
	public String getOriginalName() {
		return originalName;
	}

	/**
	 * Returns the current disguise
	 * 
	 * @return The current disguise
	 */
	public String getCurrentFakeName() {
		return currentFakeName;
	}

	/**
	 * Sets the current disguise
	 * 
	 * @param currentFakeName
	 *            The current disguise
	 */
	public void setCurrentFakeName(String currentFakeName) {
		this.currentFakeName = currentFakeName;
	}
}
