package io.github.winx64.faker.event;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import com.google.common.base.Preconditions;

import io.github.winx64.faker.Util;

/**
 * Called whenever a player disguises itself
 * 
 * @author WinX64
 *
 */
public final class PlayerFakeEvent extends PlayerEvent {

	private static final HandlerList handlers = new HandlerList();

	private final FakeReason reason;
	private String newName;
	private String newDisplayName;
	private String newPlayerListName;

	public PlayerFakeEvent(Player faker, String newName, String newDisplayName, String newPlayerListName) {
		this(faker, FakeReason.COMMAND, newName, newDisplayName, newPlayerListName);
	}

	public PlayerFakeEvent(Player faker, FakeReason reason, String newName, String newDisplayName,
			String newPlayerListName) {
		super(faker);
		this.reason = reason;
		this.newName = newName;
		this.newDisplayName = newDisplayName;
		this.newPlayerListName = newPlayerListName;
	}

	/**
	 * Returns the reason for this fake event to take place
	 * 
	 * @return
	 */
	public FakeReason getReason() {
		return reason;
	}

	/**
	 * Returns the new name of the player
	 * 
	 * @return The name
	 */
	public String getNewName() {
		return newName;
	}

	/**
	 * Sets the new name of the player. <b>Care should be taken, as the name
	 * must be unique for the entire server</b>
	 * 
	 * @param newName
	 *            The name
	 */
	public void setNewName(String newName) {
		Preconditions.checkNotNull(newName);
		Preconditions.checkArgument(Util.isNameValid(newName));
		this.newName = newName;
	}

	/**
	 * Returns the new display name of the player
	 * 
	 * @return The display name
	 */
	public String getNewDisplayName() {
		return newDisplayName;
	}

	/**
	 * Sets the new display name of the player
	 * 
	 * @param newDisplayName
	 *            The display name
	 */
	public void setNewDisplayName(String newDisplayName) {
		this.newDisplayName = newDisplayName;
	}

	/**
	 * Returns the new player list name of the player
	 * 
	 * @return The player list name
	 */
	public String getNewPlayerListName() {
		return newPlayerListName;
	}

	/**
	 * Sets the new player list name of the player. <b>Care should be taken, as
	 * the player list name must be unique for the entire server</b>
	 * 
	 * @param newPlayerListName
	 *            The player list name
	 */
	public void setNewPlayerListName(String newPlayerListName) {
		this.newPlayerListName = newPlayerListName;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	/**
	 * Causes for the disguise to take place
	 * 
	 * @author WinX64
	 *
	 */
	public static enum FakeReason {
		/**
		 * The player issued a command to disguise themselves a someone else
		 */
		COMMAND,

		/**
		 * A player with the same name as the disguise used by the player logged
		 * in. The disguise will be automatically removed to avoid conflict
		 */
		LOGIN_OVERRIDE;
	}
}
