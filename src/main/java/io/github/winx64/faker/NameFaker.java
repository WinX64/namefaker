package io.github.winx64.faker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import io.github.winx64.faker.command.CommandFake;
import io.github.winx64.faker.data.SkinCache;
import io.github.winx64.faker.listener.PlayerInOutListener;
import io.github.winx64.faker.player.FakePlayer;

/**
 * Plugin's main class
 * 
 * @author WinX64
 *
 */
public final class NameFaker extends JavaPlugin {

	/**
	 * The server version required to run the plugin. Used to check
	 * compatibility before starting anything to prevent errors
	 */
	private static final String REQUIRED_VERSION = "v1_7_R4";

	private final Map<UUID, FakePlayer> fakePlayers;
	private final SkinCache cache;
	private final Logger logger;

	public NameFaker() {
		this.fakePlayers = new HashMap<UUID, FakePlayer>();
		this.cache = new SkinCache(this);
		this.logger = getLogger();
	}

	@Override
	public void onEnable() {
		String version = Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3];
		if (!version.equals(REQUIRED_VERSION)) {
			log(Level.SEVERE, "Incompatible server implementation! Please, use %s", REQUIRED_VERSION);
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		if (!cache.loadConfiguration()) {
			log(Level.SEVERE, "Failed to load configuration. The plugin will be disable to avoid further damage!");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}
		if (!cache.initialize()) {
			log(Level.SEVERE, "Failed to initialize database. The plugin will be disable to avoid further damage!");
			Bukkit.getPluginManager().disablePlugin(this);
			return;
		}

		Bukkit.getPluginManager().registerEvents(new PlayerInOutListener(this), this);

		getCommand("fake").setExecutor(new CommandFake(this));

		for (Player player : getServer().getOnlinePlayers()) {
			this.registerFakePlayer(new FakePlayer(player));
			cache.cacheSkinData(player);
		}
	}

	@Override
	public void onDisable() {
		List<FakePlayer> fakePlayers = new ArrayList<FakePlayer>(this.fakePlayers.values());
		List<FakePlayer> pendingChange = new ArrayList<FakePlayer>();
		for (FakePlayer fPlayer : fakePlayers) {
			if (fPlayer.getCurrentFakeName() != null) {
				Player player = fPlayer.getPlayer();
				String originalName = fPlayer.getOriginalName();
				log(Level.WARNING, "[Skin] Reverting '%s' back to '%s'...", player.getName(), originalName);
				if ((Util.isNameInUse(originalName) && !player.getName().equals(originalName))
						|| (Util.isPlayerListNameInUse(originalName)
								&& !player.getPlayerListName().equals(originalName))) {
					try {
						String randomName = UUID.randomUUID().toString().replace("-", "").substring(0, 16);
						log(Level.WARNING,
								"[Skin] The name '%s' is being used by someone! Temporarily using '%s' instead!",
								originalName, randomName);
						Util.updateName(player, randomName, randomName, randomName);
						Util.updateEntityModel(cache, player, false);
						fPlayer.setCurrentFakeName(randomName);
						pendingChange.add(fPlayer);
					} catch (Exception e) {
						log(Level.SEVERE, e,
								"[Skin] An error occurred while executing the temporary rename! Details below:");
						player.kickPlayer(ChatColor.RED + "Ocorreu uma falha ao tentar remover seu disfarce!");
					}
					continue;
				}
				Util.updateName(player, fPlayer.getOriginalName(), null, null);
				Util.updateEntityModel(cache, player, false);
				fPlayer.setCurrentFakeName(null);
			}
		}

		fakePlayers = new ArrayList<FakePlayer>(this.fakePlayers.values());
		for (FakePlayer fPlayer : pendingChange) {
			Player player = fPlayer.getPlayer();
			log("[Skin] Reverting temporary '%s' back to '%s'!", player.getName(), fPlayer.getOriginalName());
			Util.updateName(player, fPlayer.getOriginalName(), null, null);
			Util.updateEntityModel(cache, player, false);
			fPlayer.setCurrentFakeName(null);
		}

		this.cache.dispose();
	}

	/**
	 * Returns the skin cache
	 * 
	 * @return The cache
	 */
	public SkinCache getCache() {
		return cache;
	}

	/**
	 * Registers a newly logged player
	 * 
	 * @param fPlayer
	 *            The new FakePlayer
	 */
	public void registerFakePlayer(FakePlayer fPlayer) {
		this.fakePlayers.put(fPlayer.getPlayer().getUniqueId(), fPlayer);
	}

	/**
	 * Returns the FakePlayer linked to the specified Player
	 * 
	 * @param player
	 *            The Player
	 * @return The FakePlayer
	 */
	public FakePlayer getFakePlayer(Player player) {
		return getFakePlayer(player.getUniqueId());
	}

	/**
	 * Returns the FakePlayer linked to the specified UUID
	 * 
	 * @param uniqueId
	 *            The UUID
	 * @return The FakePlayer
	 */
	public FakePlayer getFakePlayer(UUID uniqueId) {
		return fakePlayers.get(uniqueId);
	}

	/**
	 * Unregisters a FakePlayer that just logged out
	 * 
	 * @param fPlayer
	 *            The FakePlayer logging out
	 */
	public void unregisterFakePlayer(FakePlayer fPlayer) {
		this.fakePlayers.remove(fPlayer.getPlayer().getUniqueId());
	}

	/**
	 * Logs a formatted message of level INFO
	 * 
	 * @param format
	 *            The format
	 * @param args
	 *            The arguments
	 */
	public void log(String format, Object... args) {
		log(Level.INFO, null, String.format(format, args));
	}

	/**
	 * Logs a message of level INFO
	 * 
	 * @param message
	 *            The message
	 */
	public void log(String message) {
		log(Level.INFO, null, message);
	}

	/**
	 * Logs a formatted message of the specified level
	 * 
	 * @param level
	 *            The level
	 * @param format
	 *            The format
	 * @param args
	 *            The arguments
	 */
	public void log(Level level, String format, Object... args) {
		log(level, null, String.format(format, args));
	}

	/**
	 * Logs a message of the specified level
	 * 
	 * @param level
	 *            The level
	 * @param message
	 *            The message
	 */
	public void log(Level level, String message) {
		log(level, null, message);
	}

	/**
	 * Logs a formatted messages of the specified message along with an
	 * exception
	 * 
	 * @param level
	 *            The level
	 * @param ex
	 *            The exception
	 * @param format
	 *            The format
	 * @param args
	 *            The arguments
	 */
	public void log(Level level, Throwable ex, String format, Object... args) {
		log(level, ex, String.format(format, args));
	}

	/**
	 * Logs a message of the specified level along with an exception
	 * 
	 * @param level
	 *            The level
	 * @param ex
	 *            The exception
	 * @param message
	 *            The message
	 */
	public void log(Level level, Throwable ex, String message) {
		this.logger.log(level, message, ex);
	}
}
