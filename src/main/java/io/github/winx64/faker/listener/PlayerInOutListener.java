package io.github.winx64.faker.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.github.winx64.faker.NameFaker;
import io.github.winx64.faker.Util;
import io.github.winx64.faker.event.PlayerFakeEvent;
import io.github.winx64.faker.event.PlayerFakeEvent.FakeReason;
import io.github.winx64.faker.player.FakePlayer;

/**
 * Event listeners
 * 
 * @author WinX64
 *
 */
public final class PlayerInOutListener implements Listener {

	private static final String KICK_CONFLICTING_NAME = ChatColor.RED
			+ "Um jogador com o mesmo nome de seu disfarce se conectou. Voce foi removido do servidor!";

	private static final String WARN_CONFLICTING_NAME = ChatColor.RED
			+ "Um jogador com o mesmo nome de seu disfarce se conectou. Seu disfarce foi removido!";

	private final NameFaker plugin;

	public PlayerInOutListener(NameFaker plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent event) {
		Player player = event.getPlayer();
		Player online = Bukkit.getPlayerExact(player.getName());

		// If there's a player online by the same name, but same unique id as
		// well, it's just the player relogging
		if (online == null || player.getUniqueId().equals(online.getUniqueId())) {
			return;
		}

		// At this point, there's a player with the same name logged in, but
		// different unique id
		FakePlayer fOnline = plugin.getFakePlayer(online);

		// Is the player disguised?
		if (fOnline.getCurrentFakeName() != null) {

			// If they're, call a fake event to notify other plugins that we're
			// removing the disguise from this player. Other plugins should
			// handle the naming so it doesn't conflict
			PlayerFakeEvent fakeEvent = new PlayerFakeEvent(online, FakeReason.LOGIN_OVERRIDE,
					fOnline.getOriginalName(), null, null);
			Bukkit.getPluginManager().callEvent(fakeEvent);

			// If even after that, the player still has the same conflicting
			// name, there's no option left, kick them from the server
			if (Util.isNameInUse(fakeEvent.getNewName()) || fakeEvent.getNewName().equals(player.getName())
					|| Util.isPlayerListNameInUse(fakeEvent.getNewPlayerListName())) {
				online.kickPlayer(KICK_CONFLICTING_NAME);
				return;
			}

			// If not, remove the disguise and tell them that it has been
			// removed
			Util.updateName(online, fakeEvent.getNewName(), fakeEvent.getNewDisplayName(),
					fakeEvent.getNewPlayerListName());
			Util.updateEntityModel(plugin.getCache(), online, true);

			online.sendMessage(WARN_CONFLICTING_NAME);
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		plugin.registerFakePlayer(new FakePlayer(player));
		plugin.getCache().cacheSkinData(player);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		FakePlayer fPlayer = plugin.getFakePlayer(player);
		plugin.unregisterFakePlayer(fPlayer);
	}
}
