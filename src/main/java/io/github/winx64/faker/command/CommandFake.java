package io.github.winx64.faker.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import io.github.winx64.faker.NameFaker;
import io.github.winx64.faker.Util;
import io.github.winx64.faker.event.PlayerFakeEvent;
import io.github.winx64.faker.player.FakePlayer;

/**
 * Fake's command executor
 * 
 * @author WinX64
 *
 */
public final class CommandFake implements CommandExecutor {

	private final NameFaker plugin;

	public CommandFake(NameFaker plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String alias, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(ChatColor.RED + "Reservado para jogadores!");
			return true;
		}

		if (args.length < 1) {
			sender.sendMessage(ChatColor.RED + "Sintaxe: /fake <nome>");
			return true;
		}

		if (args[0].length() > 16) {
			sender.sendMessage(ChatColor.RED + "Seu não pode conter mais que 16 caracteres!");
			return true;
		}

		Player player = (Player) sender;
		FakePlayer fPlayer = plugin.getFakePlayer(player);
		String newName = args[0];

		if (!Util.isNameValid(newName)) {
			sender.sendMessage(ChatColor.RED + "O nome especificado não é valido!");
			return true;
		}

		PlayerFakeEvent event = new PlayerFakeEvent(player, newName, null, null);
		Bukkit.getPluginManager().callEvent(event);

		if (Util.isNameInUse(event.getNewName()) || Util.isPlayerListNameInUse(event.getNewPlayerListName())) {
			sender.sendMessage(ChatColor.RED + "Ja existe um jogador conectado com esse nome!");
			return true;
		}

		Util.updateName(player, event.getNewName(), event.getNewDisplayName(), event.getNewPlayerListName());
		Util.updateEntityModel(plugin.getCache(), player, true);
		fPlayer.setCurrentFakeName(newName);

		player.sendMessage(ChatColor.GREEN + "Voce esta disfarçado como " + ChatColor.YELLOW + newName);
		return true;
	}
}
