package io.github.winx64.faker.reflection;

import java.lang.reflect.Field;

import net.minecraft.util.com.mojang.authlib.GameProfile;

/**
 * Player related reflective methods
 * 
 * @author WinX64
 *
 */
public final class PlayerReflection {

	private static final Field GAMEPROFILE_FIELD_NAME = Reflection.getField(GameProfile.class, "name");

	/**
	 * Changes the name of a GameProfile
	 * 
	 * @param profile
	 *            The profile
	 * @param newName
	 *            The new name
	 */
	public static void setGameProfile(GameProfile profile, String newName) {
		try {
			GAMEPROFILE_FIELD_NAME.set(profile, newName);
		} catch (Exception e) {}
	}
}
