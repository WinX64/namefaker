package io.github.winx64.faker.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * General reflection utility class
 * 
 * @author WinX64
 *
 */
public final class Reflection {

	private Reflection() {}

	/**
	 * Returns the specified field, totally accessible and modifiable
	 * 
	 * @param theClass
	 *            The class the field comes from
	 * @param fieldName
	 *            The name of the field
	 * @return The field, or null if it doesn't exist
	 */
	public static Field getField(Class<?> theClass, String fieldName) {
		try {
			Field theField = theClass.getDeclaredField(fieldName);
			theField.setAccessible(true);

			Field modifiers = Field.class.getDeclaredField("modifiers");
			modifiers.setAccessible(true);
			modifiers.setInt(theField, theField.getModifiers() & ~Modifier.FINAL);

			return theField;
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Returns the specified method, totally accessible
	 * 
	 * @param theClass
	 *            The class the method comes from
	 * @param methodName
	 *            The name of the method
	 * @param parameters
	 *            The method's parameters
	 * @return The method, or null if it doesn't exist
	 */
	public static Method getMethod(Class<?> theClass, String methodName, Class<?>... parameters) {
		try {
			Method theMethod = theClass.getDeclaredMethod(methodName, parameters);
			theMethod.setAccessible(true);
			return theMethod;
		} catch (Exception e) {
			return null;
		}
	}
}
